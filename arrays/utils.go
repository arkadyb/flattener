package arrays

import (
	"fmt"
	"reflect"

	"github.com/flattener/types"
)

//FlatReflectory is used for flattening a slice of any nested level with integer values
//Slice values can be either of type int, []interface{} or []int
func FlatReflectory(inslice []interface{}) ([]int, error) {
	if len(inslice) == 0 {
		return make([]int, 0), nil
	}

	var results []int

	//Iterate over slice`s items
	for _, sliceValue := range inslice {
		sliceValueType := reflect.TypeOf(sliceValue)

		//Check type and do work
		switch sliceValueType.Kind() {
		default:
			return nil, fmt.Errorf("Expected Int, but found %s", sliceValueType.String())
		case reflect.Int:
			//For integer values, append them into results slice and return
			results = append(results, sliceValue.(int))
		case reflect.Slice:
			//For slice values, check its length and for slices of size >0, do work
			sliceLength := reflect.ValueOf(sliceValue).Len()
			if sliceLength > 0 {
				if subslice, converted := sliceValue.([]int); converted {
					//For slice`s of type []int, merge with results
					results = append(results, subslice...)
				} else if subslice, converted := sliceValue.([]interface{}); converted {
					//For slice`s of type []interface{}, recursively call Flatten function
					flatted, err := FlatReflectory(subslice)
					if err != nil {
						//For error, return nil value and error
						return nil, err
					}
					//For success, merge with results
					results = append(results, flatted...)
				} else {
					//For slices of different types, return error
					return nil, fmt.Errorf("Expected slice of type []int or []interface{}, but found %s", sliceValueType.String())
				}
			}
		}
	}

	return results, nil
}

//FlatTyped is used for flattening a nested slices of any types implementing types.Value interface
//Function returs a flatten slice of Value types or an error
func FlatTyped(inslice []types.Value) ([]types.Value, error) {
	if len(inslice) == 0 {
		return make([]types.Value, 0), nil
	}

	var result []types.Value

	//Iterate over slice values
	for _, v := range inslice {
		//Check structure type - iterable or not
		switch v.GetType() {
		default:
			//For non iterable, add value into result collection
			result = append(result, v)
		case types.IterableType:
			//Verify given iterable type is of type Slice or Array
			iterableType := reflect.TypeOf(v)
			if iterableType.Kind() == reflect.Slice || iterableType.Kind() == reflect.Array {
				arr := v.(types.Collection)
				if len(arr) > 0 { //For iterable types, check its length
					//Try to flat and collect results
					flattenCollection, err := FlatTyped(arr)
					if err != nil {
						return nil, err
					}

					result = append(result, flattenCollection...)
				}
			} else {
				//An iterable type, is not of kind Slice or Array, therefore impossible to range over it
				//Return error
				return nil, fmt.Errorf("Cant range over %s", iterableType.String())
			}
		}
	}

	return result, nil
}
