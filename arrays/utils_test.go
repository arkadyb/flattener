package arrays

import (
	"testing"

	"github.com/flattener/types"
	"github.com/stretchr/testify/assert"
)

func TestFlatReflectory(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		testSlice := []interface{}{
			1,
			2,
			[]interface{}{
				3,
				4,
				[]interface{}{
					5,
					[]int{
						6,
					},
				},
				7,
			},
			8,
			[]int{
				9,
			},
		}

		flattenSlice, err := FlatReflectory(testSlice)
		assert.NoError(t, err)
		assert.NotNil(t, flattenSlice)
		assert.Len(t, flattenSlice, 9)
		assert.EqualValues(t, []int{1, 2, 3, 4, 5, 6, 7, 8, 9}, flattenSlice)
	})

	t.Run("Empty input", func(t *testing.T) {
		flattenSlice, err := FlatReflectory(make([]interface{}, 0))
		assert.NoError(t, err)
		assert.NotNil(t, flattenSlice)
		assert.Len(t, flattenSlice, 0)
	})

	t.Run("Bad value type", func(t *testing.T) {
		//Only integers supported
		testSlice := []interface{}{
			1,
			[]interface{}{
				"Bad Value",
			},
		}

		flattenSlice, err := FlatReflectory(testSlice)
		assert.Nil(t, flattenSlice)
		assert.Error(t, err)
	})

	t.Run("Bad slice type", func(t *testing.T) {
		//Only slices of int type are supported
		testSlice := []interface{}{
			1,
			[]string{
				"Bad Value",
			},
		}

		flattenSlice, err := FlatReflectory(testSlice)
		assert.Nil(t, flattenSlice)
		assert.Error(t, err)
	})
}

type BadType int

func (bt BadType) GetType() types.Types {
	return types.IterableType
}
func TestFlatTyped(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		inSlice := []types.Value{
			types.Integer(1),
			types.Collection{
				types.Integer(2),
				types.Integer(3),
			},
			types.Collection{
				types.Integer(4),
				types.Collection{
					types.Integer(5),
					types.Collection{
						types.Collection{
							types.Integer(6),
						},
					},
				},
			},
		}

		flattenSlice, err := FlatTyped(inSlice)
		assert.NoError(t, err)
		assert.NotNil(t, flattenSlice)
		assert.Len(t, flattenSlice, 6)
		assert.Equal(t, []types.Value{types.Integer(1), types.Integer(2), types.Integer(3), types.Integer(4), types.Integer(5), types.Integer(6)}, flattenSlice)
	})

	t.Run("Empty input", func(t *testing.T) {
		flattenSlice, err := FlatTyped([]types.Value{})
		assert.NoError(t, err)
		assert.NotNil(t, flattenSlice)
		assert.Len(t, flattenSlice, 0)
	})

	t.Run("Bad iterable type", func(t *testing.T) {
		inSlice := []types.Value{
			types.Integer(1),
			types.Collection{
				BadType(2),
			},
		}

		flattenSlice, err := FlatTyped(inSlice)
		assert.Error(t, err)
		assert.Nil(t, flattenSlice)
	})
}
