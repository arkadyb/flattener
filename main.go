package main

import (
	"fmt"

	"github.com/flattener/arrays"
	"github.com/flattener/types"
)

type String string

func (s String) GetType() types.Types {
	return types.ValueType
}

func main() {

	inSlice := []interface{}{
		1,
		2,
		[]interface{}{
			3,
			4,
			[]interface{}{
				[]interface{}{
					5,
				},
				[]int{
					6,
				},
			},
			7,
		},
		8,
		[]int{
			9,
		},
	}

	flatResults, err := arrays.FlatReflectory(inSlice)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		for _, a := range flatResults {
			fmt.Printf("%d ", a)
		}
	}

	fmt.Println("")
	inSlice2 := []types.Value{
		types.Integer(1),
		types.Collection{
			types.Integer(2),
			types.Integer(3),
		},
		String("4"),
		types.Collection{
			types.Integer(5),
			types.Collection{
				types.Integer(6),
				types.Collection{
					String("7"),
					types.Collection{
						types.Integer(8),
						types.Collection{
							String("9"),
						},
					},
				},
			},
		},
	}

	flat2Results, err := arrays.FlatTyped(inSlice2)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		for _, a := range flat2Results {
			fmt.Printf("%v ", a)
		}
	}
}
