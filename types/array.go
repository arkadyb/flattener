package types

type Collection []Value

func (v Collection) GetType() Types {
	return IterableType
}
