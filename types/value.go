package types

type Types int

const (
	ValueType    Types = 1
	IterableType Types = 2
)

type Value interface {
	GetType() Types
}
